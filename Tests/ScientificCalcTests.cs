﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MyCalcLib;


namespace Calculator1.Tests
{
    class ScientificCalcTests
    {
        [TestFixture, Description("Verify the calculator function is work correctly, using <Assert> operator, and the Asserts attributes")]
        public class ScientificCalculatorUnitTest
        {
            [SetUp]
            public void Init()
            {
                ScientificCalc obj2 = new ScientificCalc();
            }

            [TearDown]
            public void CleanUp()
            {
                obj2 = null;
            }

            [Test]
            public void TestPow()
            {
                Assert.That(obj2.Pow(2, 5), Is.EqualTo(32), "2^5 should be equal to 32");
            }


            [Test, Description("Verify the correctness calculation of specified percentage of the specified number.")]
            public void TestPercent()
            {

                Assert.That(obj2.Percent(180, 10), Is.EqualTo(18), "10% from 180should be equal to 10");
            }

            [Test, Description("Verify the correctness calculation sum of elements of a one-dimensional array")]
            public void TestArrSum()
            {

                Assert.That(obj2.ArrSum(15), Is.EqualTo(15), "sum of the {0,1,2,3,4,5} array should be equal 15");
            }
            [Test, Description("Verify  the correctness of the operation of finding the minimum element in the array")]
            public void TestMinValArr()
            {
                Assert.That(obj2.MinVal(-2), Is.EqualTo(-2), "Minimum value of the of the { 9, 3, 5, 1, 10, -2 } array should be equal -2");
            }

            [Test, Description("Verify  the correctness of the operation of finding the maximum element in the array")]
            public void TestMaxValArr()
            {
                Assert.That(obj2.MaxVal(10), Is.EqualTo(10), "Maximum value of the of the { 9, 3, 5, 1, 10, -2 } array should be equal 10");
            }


            [Test, Description("Verify the correctness calculation of specified percentage of the specified number, using True attribute")]
            public void TestPercent1()
            {

                Assert.True(10 == obj2.Percent(10, 100), "10% from 100 is equal 10");
            }

            [Test, Description("Verify the correctness calculation of specified percentage of the specified number, using Less attribute")]
            public void TestPercent2()
            {
                Assert.Less(9, obj2.Percent(10, 100), "10% from 100 is equal 10");
            }

            [Test, Description("Verify the correctness of exponentiation to the specified degree, the specified number, using isFalse attribute")]
            public void TestPow1()
            {
                Assert.IsFalse(32 == obj2.Pow(2, 5), "2^5 should be equal 32");
            }
        }
    }
}
