﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MyCalcLib;


namespace Calculator1.Tests
{
    [TestFixture, Description("Verify the calculator function is work correctly, using <Assert> operator, and the Asserts attributes")]
    public class CalculatorUnitTest
    {
        MyCalc obj1 = new MyCalc();


        [Test, Description("Verify the correctness of the method of adding two integers")]

        public void TestAdd()
        {
            Assert.AreEqual(15, obj1.Add(5, 10), "5 + 10should be equal 15");
        }

        [Test, Description("Verify the correctness subtraction of one number from another number.")]
        public void TestSubs()
        {

            Assert.AreEqual(5, obj1.Subs(10, 5), "10-5 should be equal to 5");
        }

        [Test, Description("Verify the correctness subtraction of dividing one number by another number.")]
        public void TestDivide()
        {

            Assert.AreEqual(2, obj1.Divide(10, 5), "10/5 should be equal to 2");
        }

        [Test, Description("Verify the correctness of multiplying one number by another number")]
        public void TestMultiply()
        {

            Assert.AreEqual(20, obj1.Multiply(2, 10), "2*10 should be equal to 20");
        }

        [Test, Description("Verify the correctness subtraction of dividing one number by another number, using Greater attribute ")]
        public void TestDivide1()
        {
            Assert.Greater(4, obj1.Divide(50, 10), "50/10 should be equal 5");
        }

        [Test, Order(1), Description("Verify the correctness of dividing one number by another number, using constraint assertion")]
        public void TestDivide2()
        {
            Assert.That(obj1.Divide(50, 10), Is.EqualTo(5));
        }

        [Test, Description("Verify the correctness of dividing one number by another number, using constraint assertion")]
        [Ignore("Ignore a test")]
        public void TestDivideIgnore()
        {
            Assert.That(obj1.Divide(50, 10), Is.EqualTo(5));
        }
        [Test, Description("Verify the correctness subtraction of one number from another number, using NotZero attribute")]
        [Retry(8)]
        public void TestSubs1()
        {
            Assert.NotZero(obj1.Subs(4, 4), "4-4 should be equal to 0");
        }
    }    
}


