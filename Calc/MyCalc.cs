﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;


namespace MyCalcLib

{
    public class MyCalc
    {

        [TestCase(10, 5, ExpectedResult = 15, TestName = "TC#1", Description = "Checking the correctness of the summation of two numbers.")]
        [TestCase(10, 12, ExpectedResult = 15, TestName = "TC#2", Description = "Checking the correctness of the summation of two numbers, if wrong values", Author = "Mariia Abramenko")]

        public int Add(int a, int b)
        {

            return a + b;
        }

        [TestCase(10, 5, ExpectedResult = 5, TestName = "TC#3", Description = "Checking the correctness of the minus of two numbers.")]
        [TestCase(10, 5, ExpectedResult = 15, TestName = "TC#4", Description = "Checking the correctness of the minus of two numbers, if wrong values", Author = "Mariia Abramenko")]

        public int Subs(int a, int b)
        {

            return a - b;
        }

        [TestCase(10, 5, ExpectedResult = 2, TestName = "TC#5", Description = "Checking the correctness of the divide of two numbers.")]
        [TestCase(10, 5, ExpectedResult = 1, TestName = "TC#6", Description = "Checking the correctness of the divide of two numbers, if wrong values", Author = "Mariia Abramenko")]

        public int Divide(int a, int b)
        {

            return a / b;
        }

        [TestCase(10, 5, ExpectedResult = 50, TestName = "TC#7", Description = "Checking the correctness of the multiply of two numbers.")]
        [TestCase(10, 5, ExpectedResult = 10, TestName = "TC#8", Description = "Checking the correctness of the multiply of two numbers, if wrong values", Author = "Mariia Abramenko")]
        public int Multiply(int a, int b)
        {

            return a * b;
        }
    }
}
