﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;


namespace MyCalcLib
{
    class StringClass
    { 
             [Test, Description("Verify the string's is contain second string, using Contains assertion")]
            public void TestStringContain()
            {
            string phrase = "Repeat me";
            string phrase2 = "Repeat me";

            StringAssert.Contains(phrase, phrase2, "First string is contain second string");
             }

            [Test, Description("Verify the first string should be start with the second string, using StartWith assertion")]
            public void TestStringStartWith()
            {
            string phrase = "This is my first work with the NUnit Framework. So, Hello there";
            string phrase2 = "This is my first work with the NUnit Framework. So, Hello there";
            StringAssert.StartsWith(phrase, phrase2, "First string should be start with the second string.");
            }


            [Test, Description("Verify the string's are identical, using AreEqualIgnoringCase assertion.")]
            [Repeat(5)]
            public void TestStringEqual()
            {
            string phrase = "This is my first work with the NUnit Framework. So, Hello there";
            string phrase2 = "this is my first work with the nunit framework. so, hello there";
            StringAssert.AreEqualIgnoringCase(phrase, phrase2, "Lines should be equal");
            }

            [Test, Description("Verify the string's are identical, using AreEqualIgnoringCase assertion.")]
            public void TestStringIsEmpty()
            {
            string phrase = "";
            Assert.IsEmpty(phrase, "String should be empty");
            }
    }
}

