﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;


namespace MyCalcLib
{
    public class ScientificCalc
    {

        [TestCase(10, 2, ExpectedResult = 100, TestName = "TC#9", Description = "Checking the correctness of the pow")]
        [TestCase(10, 2, ExpectedResult = 10, TestName = "TC#10", Description = "Checking the correctness of pow, if wrong values", Author = "Mariia Abramenko")]
        public int Pow(int a, int n)
        {
            int k = 0;
            int b = 1;

            while (k != n)
            {
                k++;
                b *= a;
            }
            return b;

        }

        [TestCase(10, 200, ExpectedResult = 20, TestName = "TC#11", Description = "Checking the correctness of the pow")]
        [TestCase(10, 2, ExpectedResult = 10, TestName = "TC#12", Description = "Checking the correctness of pow, if wrong values", Author = "Mariia Abramenko")]
        public int Percent(int a, int b)
        {
            return a * b / 100;
        }

        public int ArrSum(int sum)
        {
            int[] numbers = new int[] { 0, 1, 2, 3, 4, 5 };
            sum = 0;
            foreach (int value in numbers)
            {
                sum += value;
            }

            return sum;
        }

        public int MinVal(int minElement)
        {
            int[] numbers = { 9, 3, 5, 1, 10, -2 };
            minElement = numbers[0];
            foreach (int element in numbers)
            {
                if (element < minElement)
                {
                    minElement = element;
                }
            }

            return minElement;
        }

        public int MaxVal(int maxElement)
        {
            int[] numbers = { 9, 3, 5, 1, 10, -2 };
            maxElement = numbers[0];
            foreach (int element in numbers)
            {
                if (maxElement < element)
                {
                    maxElement = element;
                }
            }

            return maxElement;
        }
    }
}
